# openstack

Terraform for Infomaniak OpenStack

# Configuration

Download the *clouds.yaml* file form the Openstack page (on the API Access page) and move it to */etc/openstack* or *~/.config/openstack/*.

Add your password to the *clouds.yaml* file.

# openstack cli

On Archlinux:
```zsh
sudo pacman -S python-openstackclient
```
Check installation:
```zsh
openstack --version
```

Check that everything load properly:
```zsh
openstack project list
```

# terraform

On Archlinux:
```zsh
sudo pacman -S terraform
```
Check installation:
```zsh
terraform --version
```

Adapt the *provider.tf* file to match the name of the provider of the *clouds.yaml* file.

Initialize Terraform:
```zsh
terraform init
```

Adapt the *main.tf* with the corrrect values.

Apply the configuration
```zsh
terraform apply
```
